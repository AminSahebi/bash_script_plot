#! /usr/bin/gnuplot
filename="MPI-functions-overhead"
set term png
#set output "${filename}.png"
set output sprintf('%s.png', filename)

set datafile separator ","

set yrange [0:*]      # start at zero, find max from the data
set style fill solid  # solid color boxes
unset key             # turn off all titles
set grid
myBoxWidth = 0.4
set offsets 0,0,0.5-myBoxWidth/2.,0.02
set yrange [0:15]
plot "overhead.csv" using 4:0:(0):4:($0-myBoxWidth/2.):($0+myBoxWidth/2.):(1):ytic(1) with boxxyerror lc var

